package ru.clevertec.clevertectest.viewmodel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonObject;

import javax.inject.Inject;

import androidx.databinding.BindingAdapter;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import ru.clevertec.clevertectest.data.model.Field;
import ru.clevertec.clevertectest.data.model.Form;
import ru.clevertec.clevertectest.data.model.Result;
import ru.clevertec.clevertectest.data.repository.Repository;
import ru.clevertec.clevertectest.di.scopes.AppScoped;

@AppScoped
public class FragmentViewModel extends ViewModel {

    private static MutableLiveData<Integer> progressBarImageVisibility;
    private LiveData<Form> form;
    private LiveData<Result> result;

    private final Repository repository;

    @Inject
    public FragmentViewModel(Repository repository) {
        this.repository = repository;
        progressBarImageVisibility = new MutableLiveData<>();
        progressBarImageVisibility.setValue(View.GONE);
    }


        public LiveData<Form> getForm () {
        if (form.getValue() == null) {
            this.form = repository.getForm();
        }
            return form;
        }

        public JsonObject getUserValues () {
            JsonObject userValuesField = new JsonObject();
            for (Field field : this.form.getValue().getFields()) {
                if (field.getType().equals("LIST")) {
                    userValuesField.addProperty(field.getName(), field.getValueList().get(field.getUserSelectedValuesPosition()));
                } else {
                    userValuesField.addProperty(field.getName(), field.getUserValue());
                }
            }

            JsonObject userValueForm = new JsonObject();
            userValueForm.add("form", userValuesField);
            return userValueForm;

        }

        public LiveData<Result> getResult () {
            return result;
        }

        public void sendValues (JsonObject params){
            this.result = repository.getSendValuesResponse(params);
        }

        public void CancelSendValues () {
            this.result = repository.cancelRequest();
        }


        public String getImage () {
            return this.getForm().getValue().getImage();
        }

        @SuppressLint("CheckResult") @BindingAdapter("imageUrl") public static void loadImage
        (ImageView imageView, String url){
            Context context = imageView.getContext();
            Glide.with(context).load(url).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    progressBarImageVisibility.setValue(View.GONE);
                    return false;
                }
            }).into(imageView);
        }

        public LiveData<Integer> getProgressBarImageVisibility () {
            return progressBarImageVisibility;
        }


    public void init() {
        if (result == null) {
            this.result = new MediatorLiveData<>();

        }
        if (form == null) {
            form = new MediatorLiveData<>();

        }
    }
}