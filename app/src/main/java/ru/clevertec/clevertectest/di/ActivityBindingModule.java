package ru.clevertec.clevertectest.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.clevertec.clevertectest.MainActivity;
import ru.clevertec.clevertectest.MainModule;
import ru.clevertec.clevertectest.di.scopes.ActivityScoped;


@Module
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = {MainModule.class})
    abstract MainActivity mainActivity();
}