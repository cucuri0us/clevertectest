package ru.clevertec.clevertectest.di;


import android.content.Context;

import dagger.Binds;
import dagger.Module;
import ru.clevertec.clevertectest.Application;


@Module
public abstract class AppModule {
    // expose Application as an injectable context
    @Binds
    abstract Context bindContext(Application application);
}
