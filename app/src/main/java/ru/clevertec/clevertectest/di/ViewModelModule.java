package ru.clevertec.clevertectest.di;


import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import ru.clevertec.clevertectest.di.main.MainViewModelFactory;
import ru.clevertec.clevertectest.di.scopes.AppScoped;
import ru.clevertec.clevertectest.viewmodel.FragmentViewModel;


@Module
public abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(FragmentViewModel.class)
    abstract ViewModel bindFragmentViewModel(FragmentViewModel messageViewModel);

    @Binds
    @AppScoped
    abstract ViewModelProvider.Factory bindViewModelFactory(MainViewModelFactory factory);
}