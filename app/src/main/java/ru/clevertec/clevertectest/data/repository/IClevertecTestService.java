package ru.clevertec.clevertectest.data.repository;

import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ru.clevertec.clevertectest.data.model.Form;
import ru.clevertec.clevertectest.data.model.Result;

public interface IClevertecTestService {
    String HTTP_API_CLEVERTEC_URL = "http://test.clevertec.ru/tt/";
    @POST("meta")
    Observable<Form> getForm();

    @Headers({"Content-Type: application/json","Accept: application/json"})
    @POST("data")
    Observable<Result> sendValues(@Body JsonObject params);

}
