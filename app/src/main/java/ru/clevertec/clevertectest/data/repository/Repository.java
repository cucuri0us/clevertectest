package ru.clevertec.clevertectest.data.repository;

import android.annotation.SuppressLint;

import com.google.gson.JsonObject;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.clevertec.clevertectest.data.model.Form;
import ru.clevertec.clevertectest.data.model.Result;
import ru.clevertec.clevertectest.di.scopes.AppScoped;

@AppScoped
public class Repository {
    private ClevertecTestService clevertecTestService;

    @Inject
    public Repository(ClevertecTestService clevertecTestService) {
        this.clevertecTestService = clevertecTestService;
    }

    @SuppressLint("CheckResult")
    public LiveData<Form> getForm() {

        final MutableLiveData<Form> data = new MutableLiveData<>();
        clevertecTestService.iClevertecTestService.getForm()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data::setValue, throwable -> data.setValue(null))
        ;

        return data;
    }

    @SuppressLint("CheckResult")
    public LiveData<Result> getSendValuesResponse(JsonObject params) {
        final MutableLiveData<Result> data = new MutableLiveData<>();

        clevertecTestService.iClevertecTestService
                .sendValues(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data::setValue, throwable -> data.setValue(null))
        ;

        return data;
    }

    public  LiveData<Result> cancelRequest() {
        clevertecTestService.dispatcher.cancelAll();
        return null;
    }

}
