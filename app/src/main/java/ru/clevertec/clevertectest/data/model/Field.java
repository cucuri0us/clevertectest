package ru.clevertec.clevertectest.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Field {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("values")
    @Expose
    private Map<String,String> values;

    private String userValue;

    private int userSelectedValuesPosition;

    public String getUserValue() {
        return userValue;
    }

    public void setUserValue(String userValue) {
        this.userValue = userValue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String,String> getValues() {
        return values;
    }

    public void setValues(Map<String,String> values) {
        this.values = values;
    }

    public List<String> getValueList() {
        return new ArrayList<String>(values.values());
    }

    public void setUserSelectedValuesPosition(int userSelectedValuesPosition) {
        this.userSelectedValuesPosition = userSelectedValuesPosition;
    }

    public int getUserSelectedValuesPosition() {
        return userSelectedValuesPosition;
    }
}
