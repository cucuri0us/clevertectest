package ru.clevertec.clevertectest.data.repository;


import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.clevertec.clevertectest.di.scopes.AppScoped;

@AppScoped
public class ClevertecTestService {

    IClevertecTestService iClevertecTestService;
    Dispatcher dispatcher;

    @Inject
    public ClevertecTestService() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS).build();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(dispatcher.getMaxRequests());
        httpClient.dispatcher(dispatcher);

        Retrofit retrofit;
        retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).client(client).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).baseUrl(IClevertecTestService.HTTP_API_CLEVERTEC_URL).build();

        iClevertecTestService = retrofit.create(IClevertecTestService.class);
    }
}
