package ru.clevertec.clevertectest;


import android.os.Bundle;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import ru.clevertec.clevertectest.utils.ActivityUtils;
import ru.clevertec.clevertectest.view.ui.FormFragment;

public class MainActivity extends DaggerAppCompatActivity {
    @Inject
    FormFragment mInjectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        FormFragment fragment =
                (FormFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment == null) {
            fragment = mInjectedFragment;
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.container);
        }

    }

}
