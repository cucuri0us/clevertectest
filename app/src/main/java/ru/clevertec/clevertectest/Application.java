package ru.clevertec.clevertectest;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import ru.clevertec.clevertectest.di.DaggerAppComponent;


public class Application extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}