package ru.clevertec.clevertectest;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.clevertec.clevertectest.di.scopes.FragmentScoped;
import ru.clevertec.clevertectest.view.ui.FormFragment;


@Module
public abstract class MainModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract FormFragment formFragment();
}