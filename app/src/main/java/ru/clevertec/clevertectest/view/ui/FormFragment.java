package ru.clevertec.clevertectest.view.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.DaggerFragment;
import ru.clevertec.clevertectest.R;
import ru.clevertec.clevertectest.data.model.Form;
import ru.clevertec.clevertectest.databinding.InputFormFragmentBinding;
import ru.clevertec.clevertectest.view.adapter.FieldAdapter;
import ru.clevertec.clevertectest.viewmodel.FragmentViewModel;


public class FormFragment extends DaggerFragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private FieldAdapter fieldAdapter;
    private FragmentViewModel fragmentViewModel;

    private InputFormFragmentBinding binding;
    private AlertDialog progressDialog;

//    public static FormFragment newInstance() {
//        return new FormFragment();
//    }

    @Inject
    public FormFragment() {
        // Required empty public constructor
    }

    private static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.input_form_fragment, container, false);
        binding.setFragment(this);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadFormData();
    }

    private void loadFormData() {
        progressDialog();
        progressDialog.show();
        fragmentViewModel.getForm().observe(this, form -> {
            progressDialog.cancel();
            if (form != null) {
                handleData(form);
            } else {
                handleError();
            }
        });
    }

    private void handleData(Form inputForm) {
        if (inputForm != null && inputForm.getFields().size() > 0) {
            binding.rvFields.setLayoutManager(new LinearLayoutManager(getActivity()));

            fieldAdapter = new FieldAdapter();
            binding.rvFields.setAdapter(fieldAdapter);

            binding.setFragmentvm(fragmentViewModel);
            fieldAdapter.addList(inputForm.getFields());
            getActivity().setTitle(inputForm.getTitle());
        }
    }

    private void handleError() {
        repeatLoadDialog();
    }

    public void onClickButton() {
        hideKeyboard(getActivity());
        progressDialog.show();
        fragmentViewModel.sendValues(fragmentViewModel.getUserValues()); //at first send
        fragmentViewModel.getResult().observe(this, result -> {
            progressDialog.cancel();
            if (result != null) {
                responseDialog(result.getResult());
            } else {
                responseDialog(getString(R.string.send_error_text));
            }
        });
    }

    private void responseDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message).setTitle(R.string.dialog_title).setCancelable(false).setPositiveButton(R.string.ok, null).show();
    }

    private void progressDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        builder.setView(layoutInflater.inflate(R.layout.progress_dialog, null)).setCancelable(false);
        builder.setTitle(R.string.dialog_title).setNegativeButton(getString(R.string.cancel_button_text), (dialog, which) -> fragmentViewModel.CancelSendValues());
        progressDialog = builder.create();
    }

    private void repeatLoadDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_message_repeat).setTitle(R.string.dialog_title_repeat).setCancelable(false).setPositiveButton(R.string.yes, (dialog, which) -> {
            loadFormData();
        }).setNegativeButton(R.string.cancel, (dialog, which) -> System.exit(0)).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //fragmentViewModel = ViewModelProviders.of((FragmentActivity) getActivity()).get(FragmentViewModel.class);
        fragmentViewModel = ViewModelProviders.of(this, viewModelFactory).get(FragmentViewModel.class);
        fragmentViewModel.init();
    }
}
