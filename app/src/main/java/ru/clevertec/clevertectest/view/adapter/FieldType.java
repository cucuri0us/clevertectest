package ru.clevertec.clevertectest.view.adapter;

public enum FieldType {
    TEXT(0), NUMERIC(1), LIST(2);
    int typeCode;

    FieldType(int typeCode) {
        this.typeCode = typeCode;
    }

    public int getTypeCode() {
        return typeCode;
    }

}
