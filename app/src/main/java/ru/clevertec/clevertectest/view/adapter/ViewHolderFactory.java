package ru.clevertec.clevertectest.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import ru.clevertec.clevertectest.R;
import ru.clevertec.clevertectest.databinding.RowTypeListBinding;
import ru.clevertec.clevertectest.databinding.RowTypeNumBinding;
import ru.clevertec.clevertectest.databinding.RowTypeTextBinding;

public class ViewHolderFactory {
    public static RecyclerView.ViewHolder create(ViewGroup parent, int viewType) {

        if (viewType == FieldType.TEXT.typeCode) {
            RowTypeTextBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()), R.layout.row_type_text,
                            parent, false);
            return new ViewHolderFactory.TextViewHolder(binding);

        } else if (viewType == FieldType.NUMERIC.typeCode) {
            RowTypeNumBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()), R.layout.row_type_num,
                            parent, false);
            return new ViewHolderFactory.NumViewHolder(binding);

        } else if (viewType == FieldType.LIST.typeCode) {
            RowTypeListBinding binding = DataBindingUtil
                    .inflate(LayoutInflater.from(parent.getContext()), R.layout.row_type_list,
                            parent, false);
            return new ViewHolderFactory.ListViewHolder(binding);
        }
        return null;
    }

    public static class TextViewHolder extends RecyclerView.ViewHolder {

        RowTypeTextBinding binding;

        public TextViewHolder(RowTypeTextBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class NumViewHolder extends RecyclerView.ViewHolder {

        final RowTypeNumBinding binding;

        public NumViewHolder(RowTypeNumBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder {

        final RowTypeListBinding binding;

        public ListViewHolder(RowTypeListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
