package ru.clevertec.clevertectest.view.adapter;

import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ru.clevertec.clevertectest.data.model.Field;

public class FieldAdapter extends RecyclerView.Adapter
{
    private List<Field> fieldList;

    public FieldAdapter() {
        this.fieldList = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        String fieldType = fieldList.get(position).getType();
        if (fieldType != null) {
            if (FieldType.TEXT.toString().equals(fieldType)) {
                return FieldType.TEXT.getTypeCode();
            } else if (FieldType.NUMERIC.toString().equals(fieldType)) {
                return FieldType.NUMERIC.getTypeCode();
            } else if (FieldType.LIST.toString().equals(fieldType)) {
                return FieldType.LIST.getTypeCode();
            }
        }
        return 0;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ViewHolderFactory.create(parent, viewType);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderFactory.TextViewHolder) {
            ((ViewHolderFactory.TextViewHolder) holder).binding.setField(fieldList.get(position));
            ((ViewHolderFactory.TextViewHolder) holder).binding.executePendingBindings();
        } else  if (holder instanceof ViewHolderFactory.NumViewHolder) {
            ((ViewHolderFactory.NumViewHolder) holder).binding.setField(fieldList.get(position));
            ((ViewHolderFactory.NumViewHolder) holder).binding.executePendingBindings();
        }else  if (holder instanceof ViewHolderFactory.ListViewHolder) {
            ((ViewHolderFactory.ListViewHolder) holder).binding.setField(fieldList.get(position));
            ((ViewHolderFactory.ListViewHolder) holder).binding.executePendingBindings();
        }
    }

    @Override
    public int getItemCount() {
        return fieldList.size();
    }

    public void addList(List<Field> formFields) {
        fieldList.clear();
        fieldList.addAll(formFields);
        this.notifyDataSetChanged();
    }

    public void clearList() {
        fieldList.clear();
        this.notifyDataSetChanged();
    }




}

